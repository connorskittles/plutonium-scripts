init() {
  thread zombiesAlive();
  thread onPlayerConnect();

  preCacheItem("ray_gun");
  preCacheItem("colt");
  preCacheItem("thompson");
  preCacheItem("zombie_thompson");
}

zombiesAlive() {
  while(1) {
    zombies = GetAIArray("axis");
    if(zombies.size == 0) {
      level notify("zombies all dead");
    }
    wait 0.5;
    if(zombies.size == 0) {
      level notify("zombies still dead");
    }
  }
  wait 0.05;
}

onPlayerConnect() {
  for(;;) {
    level waittill("connecting", player);
    player thread onPlayerSpawned();
  }
}

onPlayerSpawned() {
  wait 2;
  self iPrintLnBold("^3Zombie cheat made by ^2Vay^3, enjoy! :D");
  self iPrintLnBold("^3Press ^2P ^3to enable cheats");
  self thread watchForToggleCheats();
  self thread watchForRTD();
}

watchForToggleCheats() {
  cheatsOn = false;
  while(1) {
    while(cheatsOn) {  
        if(self buttonPressed("p")) {
        self iPrintLnBold("Cheats disabled");
        self.maxhealth = 100;
        self.health = self.maxhealth;
        self setClientDvar("sv_cheats", 0);
        self setClientDvar("player_sustainAmmo", 0);
        self.score = 2000;
        self notify("cheats off");
        cheatsOn = false;
        wait 3;
      }
      wait 0.1;
    }
    while(!cheatsOn) {  
      if(self buttonPressed("p")) { 
        self iPrintLnBold("Cheats enabled");
        cheatsOn = true;
        self thread cheats();
        wait 3;
      }
      wait 0.1;
    }
  } 
}

cheats() {
  self endon("cheats off");
  self iPrintLnBold("cheats threaded");
  self setClientDvar("sv_cheats", 1);
  self setClientDvar("player_sustainAmmo", 1);
  while(1) {
    self.maxhealth = 500000;
    self.health = self.maxhealth;
    playerWeps = self getWeaponsList();
    for(i=0; i < playerWeps.size; i++) {
      self giveMaxAmmo(playerWeps[i]);
    }
    if (self.score < 50000) self.score = 50000;
    wait 0.1;
  }
}

watchForRTD() {
  wait 10;

  self iPrintLnBold("Roll the Dice module loaded");
  self iPrintLnBold("Press ^3V ^7to roll the dice once per round!");

  while(1) {  
      if(self buttonPressed("v")) {
        self iPrintLnBold("Rolling the Dice!");
        self rtd();
        // level waittill("between_round_over");
        wait 5;
    }
    wait 0.1;
  }
}

rtd() {
  random = randomIntRange(0, 11);
  switch (random) {
    case 0:
      self iPrintLnBold("you rolled double movespeed!");
      self setMoveSpeedScale(2);
      level waittill("zombies still dead");
      self setMoveSpeedScale(1);
      break;
    case 1:
      self iPrintLnBold("you rolled infinite ammo!");
      self setClientDvar("player_sustainAmmo", 1);
      level waittill("zombies still dead");
      self setClientDvar("player_sustainAmmo", 0);
      break;
    case 2:
      self iPrintLnBold("you rolled points!");
      self.score += 5000;
      break;
    case 3:
      randomNumber = randomFloat(100);
      if(randomNumber > 95) {
        if(getDvar("mapname" == "pel1") || getSubStr(getDvar("mapname"), 0, 3) != "nazi") {
          self iPrintLnBold("you rolled ray gun!");
          self giveWeapon("ray_gun");
          self giveMaxAmmo("ray_gun");
          self switchToWeapon("ray_gun");
        }
      } else {
        self iPrintLnBold("you rolled thompson!");
        if(getDvar("mapname" == "nazi_zombie_prototype") || getDvar("mapname" == "nazi_zombie_asylum") || getSubStr(getDvar("mapname"), 0, 3) != "nazi") {
          self giveWeapon("thompson");
          self giveMaxAmmo("thompson");
          self switchToWeapon("thompson");
        }
        self giveWeapon("zombie_thompson");
        self giveMaxAmmo("zombie_thompson");
        self switchToWeapon("zombie_thompson");
      }
      break;
    case 4:
      self iPrintLnBold("you rolled colour vision!");
      self setClientDvar("r_debugShader", 1);
      level waittill("zombies still dead");
      self setClientDvar("r_debugShader", 0);
      break;
    case 5:
      self iPrintLnBold("you rolled fullbright!");
      self setClientDvar("r_fullbright", 1);
      level waittill("zombies still dead");
      self setClientDvar("r_fullbright", 0);
      break;
    case 6:
      self iPrintLnBold("you rolled half movespeed!");
      self setMoveSpeedScale(0.5);
      level waittill("zombies still dead");
      self setMoveSpeedScale(1);
      break;
    case 7:
      self iPrintLnBold("you rolled steal score!");
      self.score -= 2000;
      if (self.score < 0) self.score = 0;
      break;
    case 8:
      self iPrintLnBold("you rolled take weapons!");
      self takeAllWeapons();
      self giveWeapon("colt");
      self giveMaxAmmo("colt");
      self switchToWeapon("colt");
      break;
    case 9:
      self iPrintLnBold("you rolled no jump!");
      self allowJump(false);
      level waittill("zombies still dead");
      self allowJump(true);
      break;
    case 10:
      self iPrintLnBold("you rolled dark vision!");
      self setClientDvar("r_brightness", -0.35);
      wait 12;
      countdown(3);
      self setClientDvar("r_brightness", 0);
      break;
    default:
      self iPrintLnBold("you rolled crouch walk!");
      self setStance("crouch");
      self allowJump(false);
      self allowSprint(false);
      self allowStand(false);
      self allowProne(false);
      level waittill("zombies still dead");
      self allowJump(true);
      self allowSprint(true);
      self allowStand(true);
      self allowProne(true);
      break;
  }
}

countdown(amount) {
  for(i = amount; i >= 0; i--) {
    self iPrintLnBold(i);
    wait 0.1;
  }
}
