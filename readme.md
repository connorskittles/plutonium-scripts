# Plutonium WaW Zombie Scripts

This repo will hold any custom scripts that I create for Call of Duty: World at War.

## cheats.gsc

Cheats.gsc is a file used to cheat in any singleplayer level. This will work for campaign, co-op, and for zombies. To use it, load any sp level, crouch, hold aim down sight, and melee. To disable it, do the same.

This should work for all players in your lobby.

### Install Instructions

To install any of these gsc files, download them and navigate to your plutonium install. The scripts will go into your World at War singleplayer scripts folder. By default this is usually `C:\Users\{USERNAME}\AppData\Local\Plutonium\storage\t4\raw\scripts\sp`. Any scripts that you place in this folder will be automatically loaded by the game whenever you launch an sp based level.
